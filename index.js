const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const app = express();

// Configuration
app.use(cors({origin: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.status(200).json({
    result: "Sucesso na Bahia!"
  });
});

app.post('/', (req, res) => {
  res.status(200).json({
    result: "Sucesso na Bahia!"
  });
});

// Server Listen
app.listen(3000, () => {
  console.log("Server ouvindo porta 3000");
});