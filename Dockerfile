FROM node:latest

MAINTAINER Victor Caetano

ENV PORT=3000

COPY . /var/www

WORKDIR /var/www

RUN npm install

ENTRYPOINT npm start

EXPOSE $PORT